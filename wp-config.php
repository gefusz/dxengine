<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dxengine');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S<RQB#e?`^J@@u^O$Hsu12+K?k%?NQBR9/;6>$),-q0/tb5WZs:A#mNnVIHP&U{D');
define('SECURE_AUTH_KEY',  '^Fh;mf0-(3$ (i3uUnWFw18T=:zSXT#7AQupFUEy7]:@SfUIS>Boe.]3SrH:fZV.');
define('LOGGED_IN_KEY',    'IK.:WJTao-,3+L+KgB_M#m0@CkD#2ogRn<2_BvT@*fl 4pBxkB.k>t:Hf0~3JG(C');
define('NONCE_KEY',        '+o=Z=&>}B1M`hS~aURr-rh= g>-=i:qjQ<-&HcH!aEyg;1nm6%|X%Z4H}4LrKyCX');
define('AUTH_SALT',        '>&FwE;/+cH2wKe#=CyvL0[)!%vn>Dvd[Q5X,3u;20`XoLjQJ+oKL.QHs~@4Q{N1H');
define('SECURE_AUTH_SALT', 'G [<^Rf*5;@NYk nE/,Ttjh_zch-g&~[qz(sh+qQROHAkJ1GtU2)%gxzCDK[B%Ib');
define('LOGGED_IN_SALT',   'ZT}<w(rR57/(]:.M q)5w8(qqqE(8?e/({4lg?v<a;(KrED 1~`gy7iQ6mFP!;p}');
define('NONCE_SALT',       '4BzipC6BFeUPH9WaRFGot*g|(tI6&,c-??:U2v_0Xg]E:>5bks1i/.ra(dz^IF#G');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
