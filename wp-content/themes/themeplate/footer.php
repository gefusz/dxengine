<?php

/**
 * The template for displaying the footer
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer class="py-5 bg-dark">
      <div class="container">
<!--   		<nav class="sitelinks">
			<?php themeplate_footer_menu(); ?>
		</nav> -->
        <p class="m-0 text-center text-white">Copyright &copy; 2019 <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>.
			<span>Designed and developed by <a href="<?php echo esc_html( AUTHOR_URI ); ?>"><?php echo esc_html( THEME_AUTHOR ); ?></a>.</span></p>
      </div>
      <!-- /.container -->
    </footer>
	<script>
	  function loadStyle() {
	    let documentFragment = document.createDocumentFragment();

	    function constructElement(type, attributes) {
	      let element = document.createElement(type);
	      
	      for (let attribute in attributes) {
	        element[attribute] = attributes[attribute];
	      }
	     
	      return element;
	    }

	    var assetPath = '<?php echo get_stylesheet_directory_uri(); ?>/assets/css/';
	    var clientWidth = document.documentElement.clientWidth;

	    documentFragment.appendChild(constructElement('link', {
	      type: 'text/css',
	      rel: 'stylesheet',
	      href: assetPath + 'themeplate.min.css'
	    }));

	    if (clientWidth > 767) {
	      documentFragment.appendChild(constructElement('link', {
	        type: 'text/css',
	        rel: 'stylesheet',
	        media: '(min-width: 768px)',
	        href: assetPath + 'themeplate-tablet.min.css'
	      }));
	    }

	    
	    if (clientWidth > 1199) {
	      documentFragment.appendChild(constructElement('link', {
	        type: 'text/css',
	        rel: 'stylesheet',
	        media: '(min-width: 1200px)',
	        href: assetPath + 'themeplate-desktop.min.css'
	      }));
	    }

	    document.body.appendChild(documentFragment);
	  };

	  if (window.requestAnimationFrame) {
	    window.requestAnimationFrame(function() {
	      window.setTimeout(loadStyle, 0);
	    });
	  } else {
	    window.addEventListener("DOMContentLoaded", loadStyle);
	  }
	</script>

	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
	<?php wp_footer(); ?>
</body>
</html>
