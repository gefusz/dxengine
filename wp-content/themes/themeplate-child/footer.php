<?php

/**
 * The template for displaying the footer
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

</div><!-- .site-content -->

<!-- <?php get_sidebar( 'footer' ); ?> -->

    <!-- Footer -->
    <footer id="footer" class="bg-dark">
      <div class="footer-content">
        <div class="container">
          <div class="row">
            <div class="col-lg-6">
              <h3>Sitemap</h3>
              <nav class="sitelinks">
                <?php themeplate_footer_menu(); ?>
              </nav> 
            </div>
            <div class="col-lg-6">
              
            </div>
          </div>
    		
          
          
        
        </div>
      </div>
      <div class="closing-line">
        <div class="container">
  <!--      <nav class="sitelinks">
        <?php themeplate_footer_menu(); ?>
      </nav> -->
          
            <div class="copyright">
              <p class="m-0 text-center text-white">Minden jog fenntartva &copy; <?php echo date('Y'); ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php bloginfo( 'name' ); ?></a>.</p>
            </div>
            <div class="cookie u-text-center">
              <p><a href="https://lgr-bau-epitoipar.hu/wp-content/uploads/2021/03/adatkezelesi-tajekoztato-lgr-bau.pdf" target="_blank">Adatvédelem</a><span> | </span><a href="<?php echo site_url();?>/#moove_gdpr_cookie_modal" target="_blank">Cookie beállítások</a></p>
            </div>
            <div class="designer-box">
                <div class="designer">
                  <p><?php _e('Design by ', 'themeplate-child'); ?></p>
                  <a href="https://dxlabz.hu/"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dx-logo-white.svg" alt="Dx-Labz"/></a>
                </div>
            </div>

        </div>
        <!-- /.container -->
      </div>
      <!-- /.container -->
      <button onclick="topFunction()" id="scroll-to-top" title="Go to top"><img class="top-btn" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/to-top.svg" alt="fel gomb"/></button>
    </footer>

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" async></script>
	<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js" defer></script>
  <!-- <script src="https://cdn.jsdelivr.net/npm/jquery@3.4.1/dist/jquery.min.js"></script> -->
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<!--SCROLL TO TOP BUTTON  -->
  <script>
    //Get the button:
      mybutton = document.getElementById("scroll-to-top");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      function topFunction() {
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
      }
  </script>
<!------------------------->

<!-- COUNTDOWN FOR SPECIAL OFFERS  -->

<!-- <?php  if (is_front_page()) { ?>
  <script>
 
  // Set the date we're counting down to
  var countDownDate = new Date("Dec 31, 2020 15:37:25").getTime();

  // Update the count down every 1 second
  var x = setInterval(function() {

    // Get today's date and time
    var now = new Date().getTime();

    // Find the distance between now and the count down date
    var distance = countDownDate - now;

    // Time calculations for days, hours, minutes and seconds
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);

    // Display the result in the element with id="demo"
    document.getElementById("timer").innerHTML = days + "d " + hours + "h "
    + minutes + "m " + seconds + "s ";

    // If the count down is finished, write some text
    if (distance < 0) {
      clearInterval(x);
      document.getElementById("timer").innerHTML = "EXPIRED";
    }
  }, 1000);
</script>
<?php } ?> -->
<!------------------------->

<!-- GYIK ELEMENT SHOW - HIDE -->
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
     var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}
</script>
<!------------------------->


<!-- MULTIPLE SUB-MENU ELEMENT SHOW - HIDE -->
  <script>
  jQuery( document ).ready(function() {
    jQuery('.menu-item-has-children').click(function(){
    jQuery(this).toggleClass('open').siblings('.menu-item-has-children');
  });
  });
</script>
<!------------------------->


<!-- SCROLLING WILL MAKE THE HEADER SMALLER -->
  <script>
    jQuery(document).ready(function() {
      if (jQuery(window).width() > 991) {
        jQuery(document).scroll(function() {
          if (jQuery(window).scrollTop() - jQuery('body').offset().top > 100) {
            jQuery('div.topbar').addClass('hide-on-desktop');
            jQuery('nav').addClass('small');
          } else {
            jQuery('div.topbar').removeClass('hide-on-desktop');
            jQuery('nav').removeClass('small');
          }
        });
      }
    });
  </script>
<!------------------------->

	<?php wp_footer(); ?>
</body>
</html>
