<?php
/**
 * The main template file
 *
 * @package ThemePlate
 * @since 0.1.0
 */

get_header();

?>

<!-- Page Content -->

<section id="slider">
  <?php get_template_part( 'template-parts/carousel', 'full-width' ); ?>
</section>

<section id="excellence">
	<div class="container">
		<p>Több <span>1</span>, több <span>2</span>, több <span>3</span>, hogy örökre elfelejthesd az aggódást</p>
	</div>
</section>

<section id="featured-testimonial">
	<div class="container">
		<div class="row">
			<div class="col-lg-8">
				<p class="text">"Kipróbáltam amit kínáltak, és az eredmények még legvadabb álmaimat is felülmúlták. Nem győzök hálát adni a sorsnak, hogy rájuk találtam."</p>
			</div>
			<div class="col-lg-2">
				<img class="img" src="https://placehold.it/700x400"></a>
			</div>
			<div class="col-lg-2">
				<h5 class="name">Sikeres Sándor</h5>
				<p class="title">CEO, Success Inc.</p>
			</div>
		</div>
	</div>
</section>

<section id="clients">
  <div class="container">
    <h2 class="section-title bright">Cégek sora, akiknek nagy sikereket értünk el</h2>
      <hr class="underline bright">
      <div class="bs-example">
        <div id="clientsCarousel" class="carousel slide" data-ride="carousel">
            <!-- Carousel indicators -->
            <ol class="carousel-indicators">
                <li data-target="#clientsCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#clientsCarousel" data-slide-to="1"></li>
            </ol>
            <!-- Wrapper for carousel items -->
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div id="img-gallery">
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                    </div>
                </div>
                <div class="carousel-item">
                    <div id="img-gallery">
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                      <?php picture('logo-placeholder', 'png', 'logo-placeholder',  true, 'lazy'); ?>
                    </div>       
                </div> 
            </div>
              <!-- Carousel controls -->
<!--               <a class="carousel-control-prev" href="#clientsCarousel" data-slide="prev">
                  <span class="carousel-control-prev-icon"></span>
              </a>
              <a class="carousel-control-next" href="#clientsCarousel" data-slide="next">
                  <span class="carousel-control-next-icon"></span>
              </a> -->
        </div>
      </div>
  </div>
</section>

<section id="featured-blog">
  <div class="container">
    <h2 class="section-title">Featured Blog</h2>
 	<hr class="underline">
    <!-- Marketing Icons Section -->
    <div class="row">

      <?php 
        $args_featured_blog = array(
          'post_type' => 'post',
          'posts_per_page' => 1,
          'orderby' => 'post_date',
          'order' => 'DESC',
        );

        $loop_featured_blog = new WP_Query( $args_featured_blog );
      ?>
      <?php if ( $loop_featured_blog->have_posts() ) : ?>
        <?php while ( $loop_featured_blog->have_posts() ) : ?>
          <?php $loop_featured_blog->the_post(); ?>

          <div class="col-lg-6 mb-6">
            <div class="card h-100">
              <div class="card-img">
              <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
              <?php } else { ?>
                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
              <?php } ?>
              </div>
            </div>
          </div>
           <div class="col-lg-6 mb-6">
              <h4 class="card-header">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h4>
              <div class="card-body">
                <p class="card-text">
                  <div class="entry-content">
                    <?php the_excerpt(); ?>
                  </div></p>
                </div>
                <div class="card-footer">
                  <a href="<?php the_permalink(); ?>" class="c-btn c-btn--primary">Read More</a>
                </div>
          </div>
    </div><!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <!-- /.row -->
  </div>
</section>

<section id="solution">
  <div class="container">
    <h2 class="section-title bright">A Te problémádat mi igazán megértjük mert napi szinten talákoztunk vele</h2>
    <hr class="underline bright">
    <p class="description">Mi is jártunk ott ahol te, ezért közös nevezőn vagyunk, innen tudjuk hogy ez mennyire fájdalmas neked. Azonban hosszas kutatás és rengeteg próbálkozás után végül megtaláltuk a kiutat. Azóta azon dolgozunk, hogy minél több sorstársunkhoz jutassuk el ezt a megoldást, hogy elkerülhessék azokat a hibákat amit mi meg kellet, hogy tapasztaljunk.</p>
    <div class="row">
      <div class="col-lg-6 text">
        <h5 class="bright">Amikor előszört találkoztunk a problémával kiltátástalannak tűnt a helyzet</h5>
        <p>Először még nem is érzékeltük a problémát, csak el szerettünk volna indulni és azt vettük észre, hogy nehezen mennek a dolgok. Úgy gondoltuk, hogy idővel így is működni fognak a dolgok, de sajnos a tények mást mutattak. Ekkor éreztük először, hogy sürgősen változtatnunk kell mert különben nagyon sok veszteség érhet minket.</p>
      </div>
      <div class="col-lg-6 img">
        <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
      </div>
      <div class="col-lg-6 img">
        <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
      </div>
      <div class="col-lg-6 text">
        <h5 class="bright">Amikor előszört találkoztunk a problémával kiltátástalannak tűnt a helyzet</h5>
        <p>Először még nem is érzékeltük a problémát, csak el szerettünk volna indulni és azt vettük észre, hogy nehezen mennek a dolgok. Úgy gondoltuk, hogy idővel így is működni fognak a dolgok, de sajnos a tények mást mutattak. Ekkor éreztük először, hogy sürgősen változtatnunk kell mert különben nagyon sok veszteség érhet minket.</p>
      </div>
    </div>
  </div>
</section>

<section id="blog">
  <div class="container">

    <h2 class="section-title">Blog</h2>
 	<hr class="underline">
    <!-- Marketing Icons Section -->
    <div class="row">

      <?php 
        $args_blog = array(
          'post_type' => 'post',
          'posts_per_page' => 3,
          'orderby' => 'post_date',
          'order' => 'DESC',
        );

        $loop_blog = new WP_Query( $args_blog );
      ?>
      <?php if ( $loop_blog->have_posts() ) : ?>
        <?php while ( $loop_blog->have_posts() ) : ?>
          <?php $loop_blog->the_post(); ?>

          <div class="col-lg-4 mb-4">
            <div class="card h-100">
              <div class="card-img">
              <?php if ( has_post_thumbnail() ) { ?>
                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('post-thumbnail', ['class' => 'card-img-top']); ?></a>
              <?php } else { ?>
                <a href="<?php the_permalink(); ?>"><img src="https://placehold.it/700x400"></a>
              <?php } ?>
              </div>
              <h4 class="card-header">
                <a href="<?php the_permalink(); ?>">
                  <?php the_title(); ?>
                </a>
              </h4>
              <div class="card-body">
                <p class="card-text">
                  <div class="entry-content">
                    <?php the_excerpt(); ?>
                  </div></p>
                </div>
                <div class="card-footer">
                  <a href="<?php the_permalink(); ?>" class="c-btn c-btn--primary">Read More</a>
                </div>
              </div>
            </div><!-- #post-<?php the_ID(); ?> -->

          <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
      </div>
      <!-- /.row -->
  </div>
</section>

<section id="gallery">
  <div class="container">
    <h2 class="section-title">Gallery</h2>
    <hr class="underline">
    <div class="row">
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
			<?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-3.jpg">
			<?php picture('placeholder-img-3', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
			<?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-3.jpg">
			<?php picture('placeholder-img-3', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
			<?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
      <div class="col-lg-4 gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-3.jpg">
			<?php picture('placeholder-img-3', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
    </div>
    
  </div>
</section>

<section id="gallery-masonry">
  <div class="container">
    <h2 class="section-title">Masonry Gallery</h2>
    <hr class="underline">
    <div class="row">
      <div class="masonry gallery-img-box">
        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
      <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
      <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-image-portrait.png">
      <?php picture('placeholder-image-portrait', 'png', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-image-portrait.png">
      <?php picture('placeholder-image-portrait', 'png', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-image-portrait.png">
      <?php picture('placeholder-image-portrait', 'png', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
      <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-image-portrait.png">
      <?php picture('placeholder-image-portrait', 'png', 'placeholder',  true, 'lazy'); ?>
        </a>

        <a data-fancybox="gallery" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/placeholder-img-2.jpg">
      <?php picture('placeholder-img-2', 'jpg', 'placeholder',  true, 'lazy'); ?>
        </a>
      </div>
    </div>
  </div>
</section>

<section id="offer">
  <div class="container">
      <h2 class="section-title">Van rád szabott ajánlatunk, mert tudjuk hogy mindenki más-más tempóban halad</h2>
      <hr class="underline">
      <div class="row">
        <div class="col-lg-4 offer-box">
          <h4 class="offer-title">Opció B</h4>
          <ul>
            <li>Hihetetlen tulajdonság 1</li>
            <li>Hihetetlen tulajdonság 2</li>
            <li>Hihetetlen tulajdonság 3</li>
            <li>Hihetetlen tulajdonság 4</li>
            <li>+ Óriási bónusz</li>
          </ul>
          <h5 class="offer-price">$999</h5>
          <button class="c-btn c-btn--primary">Megrendelem</button>
        </div>
        <div class="col-lg-4 offer-box">
          <h4 class="offer-title">Opció A</h4>
          <ul>
            <li>Hihetetlen tulajdonság 1</li>
            <li>Hihetetlen tulajdonság 2</li>
            <li>Hihetetlen tulajdonság 3</li>
          </ul>
          <h5 class="offer-price">$666</h5>
          <button class="c-btn c-btn--primary">Megrendelem</button>
        </div>
        <div class="col-lg-4 offer-box">
          <h4 class="offer-title">Opció C</h4>
          <ul>
            <li>Hihetetlen tulajdonság 1</li>
            <li>Hihetetlen tulajdonság 2</li>
          </ul>
          <h5 class="offer-price">$555</h5>
          <button class="c-btn c-btn--primary">Megrendelem</button>
        </div>
      </div>
  </div>
</section>
<section id="countdown">
  <div class="container">
      <h2 class="section-title">Díjazzuk a gyorsaságot</h2>
      <hr class="underline">
      <p>Ha most rendelsz akkor</p>
      <ul>
        <li>+ Óriási bónusz</li>
        <li>+ Óriási bónusz</li>
        <li>+ Óriási bónusz</li>
      </ul>
      <p>Már csak ennyi időd maradt</p>
      <h4 id="timer"></h4>
      <button class="c-btn c-btn--primary">Kipróbálom</button>
  </div>
</section>

<section id="our-team">
	<div class="container">
	    <!-- Our Team Section -->
	    <h2 class="section-title">Our Team</h2>
	    <hr class="underline">
    <div class="row-holder">
	    <div class="row">
        <?php 
        $args_our_team = array(
          'post_type' => 'our_team',
          'posts_per_page' => 4,
          'orderby' => 'title',
          'order' => 'ASC',
        );

        $loop_our_team = new WP_Query( $args_our_team );
      ?>
      <?php if ( $loop_our_team->have_posts() ) : ?>
        <?php while ( $loop_our_team->have_posts() ) : ?>
          <?php $loop_our_team->the_post(); ?>
	         <div class="col-lg-3 col-sm-6 team-member-box">
            <?php if ( has_post_thumbnail() ) { ?>
                <?php the_post_thumbnail('medium', ['class' => 'member-img']); ?>
              <?php } ?>
	            <h4 class="member-name"><?php the_title(); ?></h4>
	            <p class="member-title"><?php the_field('titulus'); ?></p>
	            <p class="member-description"><?php the_content(); ?></p>
	          </div>
        <?php endwhile; ?>
        <?php endif; ?>
        <?php wp_reset_postdata(); ?>
	      </div>
	    </div>
	    <!-- /.row -->
	</div>
</section>

<section id="testimonial">
<div class="container">
    <h2 class="section-title">Testimonial</h2>
    <hr class="underline">
    <div class="bs-example">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
        	                <?php
            $args_testimonials = array(
                'post_type' => 'testimonial',
                'posts_per_page' => -1,
                'orderby' => 'title',
          		'order' => 'ASC',
            );
			$post_counter = 0;
			$indicators_counter = 0;
            $loop_testimonials = new WP_Query( $args_testimonials );
        if ( $loop_testimonials->have_posts() ) : while ( $loop_testimonials->have_posts() ) : $loop_testimonials->the_post();
        	$post_counter++;
		if ($post_counter == 1) { ?>
	            <div class="carousel-item active">
                <div class="testimonial">
					<div class="row person-data">
						<div class="col-lg-12 testimonial--name-and-title">
							<h4 class="testimonial--name"><?php the_title(); ?></h4>
							<h5 class="testimonial--title"><?php the_field('testimonial_titulus'); ?></h5>
						</div>
						<div class="col-12 testimonial--content">
							<?php
								$content = get_the_content();
								$content = '<span>„</span>'. $content .'<span>”</span>';
								echo apply_filters('the_content', $content);
							?>
						</div>
					</div>
				</div>
            </div>
			<?php } else { ?>
            <div class="carousel-item">
                <div class="testimonial">
					<div class="person-data">
						<div class="col-lg-12 testimonial--name-and-title">
							<h4 class="testimonial--name"><?php the_title(); ?></h4>
							<h5 class="testimonial--title"><?php the_field('testimonial_titulus'); ?></h5>
						</div>
						<div class="col-12 testimonial--content">
							<?php
								$content = get_the_content();
								$content = '<span>„</span>'. $content .'<span>”</span>';
								echo apply_filters('the_content', $content);
							?>
						</div>
					</div>
				</div>
            </div>
			<?php } ?>
           <?php endwhile; ?>
          <?php endif; ?>
          <?php wp_reset_postdata(); ?>
        </div>
        <ol class="carousel-indicators">
            <?php
            for($indicator_number = 0; $indicator_number < $post_counter; $indicator_number++) {
            	if ($indicator_number == 0 ){
            		echo "<li data-target='#myCarousel' data-slide-to='".$indicator_number."' class='active'></li>";
            	} else {
            		echo "<li data-target='#myCarousel' data-slide-to='".$indicator_number."'></li>";
            	}
        	 }?>
        </ol>
        <!-- Carousel controls -->
        <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" data-slide="next">
            <span class="carousel-control-next-icon"></span>
        </a>
        </div>
    </div>
</div>
</section>
  <section id="gyik" class="bg-dark">
    <div class="container">
      <h2 class="section-title">Gyakran ismételt Kérdések</h2>
      <hr class="underline">
      <div class="row">
        <div class="col-lg-12 gyik-content">
          <?php 
            $args_gyik = array(
              'post_type' => 'gyik',
              'posts_per_page' => 5,
              'orderby' => 'date',
              'order'   => 'DESC',
            );

            $loop_gyik = new WP_Query( $args_gyik );
          ?>
          <?php if ( $loop_gyik->have_posts() ) : ?>
            <?php while ( $loop_gyik->have_posts() ) : ?>
              <?php $loop_gyik->the_post(); ?>
                <div class="accordion">
                  <!-- <img class="decoration" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/nyito-jel.svg" alt="gyik"/> -->
                  <h4 class="question"><?php the_title(); ?></h4>
                </div>
                <div class="panel">
                  <p><?php the_content(); ?></p>
                </div>

            <?php endwhile; ?>
            <?php endif; ?>
            <?php wp_reset_postdata(); ?>
        <div class="u-text-center">
          <a href="<?php echo site_url(); ?>/#" class="c-btn c-btn-cprimary">Minden kérdés</a>
        </div>
        </div>
      </div>
    </div>
    
  </section>

<section id="contact">
	<div class="container">
		<h2 class="section-title">Contact Us</h2>
		<hr class="underline">
		<?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]'); ?>
	</div>
</section>


<section id="partners">
	<div class="container">
		<h2 class="section-title">Partners</h2>
		<hr class="underline">
		<div class="logos">
			<?php
			    $args_partners = array(
			        'post_type' => 'partner',
			        'posts_per_page' => -1,
			    );
			    $loop_partners = new WP_Query( $args_partners );
			if ( $loop_partners->have_posts() ) : while ( $loop_partners->have_posts() ) : $loop_partners->the_post(); ?>
			  <?php echo get_the_post_thumbnail( $post->ID, 'medium', ['class' => 'partner-logo-img'] ); ?>
			  <?php endwhile; ?>
			  <?php endif; ?>
        <?php wp_reset_postdata(); ?>
		</div>
	</div>
</section>


  <?php

  get_footer();
