<?php

/**
 * The template for displaying the banner
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

<div class="language-selector">
    <?php

    $languages = array('hu','en','de','it'); ?>
    <button onclick="languageSelector()" class="dropdown-toggle">
     	<?php 
            $language_options = pll_the_languages(array('raw' => 1));

            foreach ($language_options as $lang) {
              if ($lang['current_lang']) {
                echo '<img src="' . $lang['flag'] . '"/>';
              } 
            } 
        ?>  
    </button>
    <div class="languages">
      	<?php 
          	foreach ($language_options as $lang) {
            	if ($lang['slug'] != $lang['current_lang']) {
            		echo '<a class="dropdown-item" href="' . $lang['url'] .'" data-value="' . $lang['slug'] . '"' . ($lang['current_lang'] ? 'selected="selected"' : '') .  '><img class="language-flag flag-'. $lang['name'] . '"src="'. $lang['flag'] . '" alt="'. $lang['name'] . '"></a>';
            	}
          	}
        ?>
    </div> 
</div>
