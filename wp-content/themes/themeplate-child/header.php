<?php

/**
 * The template for displaying the header
 *
 * @package ThemePlate
 * @since 0.1.0
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://fonts.gstatic.com" rel="preconnect" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com/" crossorigin>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/themeplate.min.css?v=<?php echo time(); ?>">
    <script>
      var pluginsUrl = '<?php echo plugins_url(); ?>';
      var themePath = '<?php echo get_stylesheet_directory_uri(); ?>';

      function constructElement(type, attributes) {
        let element = document.createElement(type);

        for (let attribute in attributes) {
          element[attribute] = attributes[attribute];
        }

        return element;
      }

      function loadBaseStyles() {
        let documentFragment = document.createDocumentFragment();

        var clientWidth = document.documentElement.clientWidth;

        if (clientWidth > 767) {
          documentFragment.appendChild(constructElement('link', {
            rel: 'stylesheet',
            media: '(min-width: 768px)',
            href: themePath + '/assets/css/themeplate-tablet.min.css<?php echo '?v=' . time(); ?>'
          }));
        }


        if (clientWidth > 1199) {
          documentFragment.appendChild(constructElement('link', {
            rel: 'stylesheet',
            media: '(min-width: 1200px)',
            href: themePath + '/assets/css/themeplate-desktop.min.css<?php echo '?v=' . time(); ?>'
          }));
        }

        document.head.appendChild(documentFragment);
      }

      loadBaseStyles();
    </script>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class(WEBP ? 'webP' : ''); ?>>
    <?php wp_body(); ?>

<!--    <a class="screen-reader-text" href="#site-content">--><?php //esc_html_e( 'Skip to content', 'themeplate' ); ?><!--</a>-->

    <header>
      <!-- Search -->
      <div class="topbar hide-on-mobile">
        <div class="container"> 
          <div class="telephon-box hide-on-mobile">
            <img class="telephon" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/tel-icon.svg" alt="telefon"/>
            <a href="+36303362320">+36 30 336 2320</a>
          </div>
          <div class="email-box hide-on-mobile">
            <img class="email" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/email-icon.svg" alt="email"/>
            <a href="mailto:info@dxlabz.hu">info@dxlabz.hu</a>
          </div>  
          <div class="facebook-box hide-on-mobile">
            <a href="https://www.facebook.com/dxlabz"><img class="facebook" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/facebook-icon.svg" alt="facebook"/></a>
          </div> 
           <div class="search-box">
            <?php echo do_shortcode('[wpbsearch]'); ?>
          </div>
          <?php
          /**** HA TÖBBNYELVŰ OLDALT KÉSZÍTÜNK A NYELVVÁLASZTÓ 
          FUNKCIÓ MEGHÍVÁSÁHOZ ENGEDÉLYEZZÜK A NYELVVÁLASZTÓT TARTALMAZÓ FÁJL BETÖLTÉSÉT - POLYLANG ****/
            //get_template_part( '/template-parts/language-selector' );
          ?>
        </div>
      </div>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
          <a href="<?php bloginfo('url'); ?>" class="navbar-brand"><img class="" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/dx-logo-white.svg" alt="Dx-Labz"/></a>
          <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <?php themeplate_primary_menu(); ?>
          </div>
        </div>
      </nav>
    </header>

